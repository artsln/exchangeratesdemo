﻿using System;
using System.Collections.Generic;
using System.Linq;
using ExchangeRatesTestsApi.Helpers;
using ExchangeRatesTestsApi.Models;
using FluentAssertions;
using RestSharp;
using TechTalk.SpecFlow;

namespace ExchangeRatesTestsApi.Steps
{
    [Binding]
    public class LatestForeignExchangeRatesSteps
    {
        private RestClient _restClient;
        private RestRequest _restRequest;
        private IRestResponse _restResponse;
        private RatesResponse _jsonRatesResponse;
        private ErrorResponse _jsonErrorResponse;
        private double _inverselyProportionalFirstPairValue;
        private double _inverselyProportionalSecondPairValue;

        private readonly IEnumerable<string> _allSupportedCurrencies = RatesHelper.GetAllSupportedCurrencies();

        [Given(@"I set URL to /latest")]
        public void GivenISetUrlToLatest()
        {
            _restClient = RestApiHelper.SetUrl("/latest");
        }

        [When(@"I send HEAD request")]
        public void WhenISendHeadRequest()
        {
            _restRequest = RestApiHelper.CreateHeadRequest();
            _restResponse = RestApiHelper.GetResponse(_restClient, _restRequest);
        }

        [When(@"I send PUT request")]
        public void WhenISendPutRequest()
        {
            _restRequest = RestApiHelper.CreatePutRequest();
            _restResponse = RestApiHelper.GetResponse(_restClient, _restRequest);
        }

        [When(@"I send POST request")]
        public void WhenISendPostRequest()
        {
            _restRequest = RestApiHelper.CreatePostRequest();
            _restResponse = RestApiHelper.GetResponse(_restClient, _restRequest);
        }

        [When(@"I send DELETE request")]
        public void WhenISendDeleteRequest()
        {
            _restRequest = RestApiHelper.CreateDeleteRequest();
            _restResponse = RestApiHelper.GetResponse(_restClient, _restRequest);
        }

        [When(@"I send GET request")]
        public void WhenISendGetRequest()
        {
            _restRequest = RestApiHelper.CreateGetRequest();
            _restResponse = RestApiHelper.GetResponse(_restClient, _restRequest);
            _jsonRatesResponse = _restResponse.GetContent<RatesResponse>();
        }

        [When(@"I send GET request with symbols parameter set to ""(.*)""")]
        public void WhenISendGetRequestWithSymbolsParameterSetTo(string symbols)
        {
            _restRequest = RestApiHelper.CreateGetRequest(symbols: symbols);
            _restResponse = RestApiHelper.GetResponse(_restClient, _restRequest);
            _jsonRatesResponse = _restResponse.GetContent<RatesResponse>();
        }

        [When(@"I send GET request with base parameter set to ""(.*)""")]
        public void WhenISendGetRequestWithBaseParameterSetTo(string baseCurrency)
        {
            _restRequest = RestApiHelper.CreateGetRequest(baseCurrency);
            _restResponse = RestApiHelper.GetResponse(_restClient, _restRequest);
            _jsonRatesResponse = _restResponse.GetContent<RatesResponse>();
        }

        [When(@"I send GET request with non-existing base parameter set to ""(.*)""")]
        public void WhenISendGetRequestWithNonExistingBaseParameterSetTo(string baseCurrency)
        {
            _restRequest = RestApiHelper.CreateGetRequest(baseCurrency);
            _restResponse = RestApiHelper.GetResponse(_restClient, _restRequest);
            _jsonErrorResponse = _restResponse.GetContent<ErrorResponse>();
        }

        [When(@"I send GET request with non-existing symbols parameter set to ""(.*)""")]
        public void WhenISendGetRequestWithNonExistingSymbolsParameterSetTo(string symbols)
        {
            _restRequest = RestApiHelper.CreateGetRequest(symbols: symbols);
            _restResponse = RestApiHelper.GetResponse(_restClient, _restRequest);
            _jsonErrorResponse = _restResponse.GetContent<ErrorResponse>();
        }

        [When(@"I send GET request with both symbols and base parameter set to base: ""(.*)"" and symbol: ""(.*)""")]
        public void WhenISendGetRequestWithBothSymbolsAndBaseParameterSetToBaseAndSymbol(string baseCurrency, string symbols)
        {
            _restRequest = RestApiHelper.CreateGetRequest(baseCurrency, symbols);
            _restResponse = RestApiHelper.GetResponse(_restClient, _restRequest);
            _jsonRatesResponse = _restResponse.GetContent<RatesResponse>();
        }

        [When(@"I save ""(.*)"" currency rate from response for ""(.*)""")]
        public void WhenISaveCurrencyRateFromResponseFor(string currencyPairValue, string currency)
        {
            if (currencyPairValue == "firstPairValue")
                _inverselyProportionalFirstPairValue = Convert.ToDouble(_jsonRatesResponse.GetCurrencyValue(currency));
            else
                _inverselyProportionalSecondPairValue = Convert.ToDouble(_jsonRatesResponse.GetCurrencyValue(currency));
        }


        [Then(@"Currency rates are inversely proportional")]
        public void ThenCurrencyRatesAreInverselyProportional()
        {
            const int precisionDigits = 4;
            var isInverselyProportionalFirstPairValue = Math.Round(1 / _inverselyProportionalFirstPairValue, precisionDigits) ;
            var isInverselyProportionalSecondPairValue = Math.Round(1 / _inverselyProportionalSecondPairValue, precisionDigits);

            isInverselyProportionalFirstPairValue
                .Should()
                .Be(Math.Round(_inverselyProportionalSecondPairValue, precisionDigits));

            isInverselyProportionalSecondPairValue
                .Should()
                .Be(Math.Round(_inverselyProportionalFirstPairValue, precisionDigits));
        }

        [Then(@"HTTP status should be (.*)")]
        public void ThenHttpStatusShouldBe(int expectedStatusCode)
        {
            _restResponse.GetStatusCode()
                .Should()
                .Be(expectedStatusCode);
        }
        
        [Then(@"Body should be empty")]
        public void ThenBodyShouldBeEmpty()
        {
            _restResponse.Content
                .Should()
                .BeNullOrEmpty();
        }

        [Then(@"Body should be ""(.*)""")]
        public void ThenBodyShouldBe(string expectedBodyContent)
        {
            _restResponse.Content
                .Should()
                .Be(expectedBodyContent);
        }

        [Then(@"Content-Type should be ""(.*)""")]
        public void ThenContentTypeShouldBe(string expectedBodyContentType)
        {
            _restResponse.ContentType
                .Should()
                .Be(expectedBodyContentType);
        }

        [Then(@"Base Currency should be ""(.*)""")]
        public void ThenBaseCurrencyShouldBe(string expectedBaseCurrency)
        {
            _jsonRatesResponse.@base
                .Should()
                .Be(expectedBaseCurrency);
        }

        [Then(@"Date should not be empty")]
        public void ThenDateShouldNotBeEmpty()
        {
            // can't assert that /latest date it is today
            // because it takes info from ECB each day after ~16.00 ETC
            _jsonRatesResponse.date
                .Should()
                .NotBeNullOrEmpty();
        }

        [Then(@"In response should be all currencies")]
        public void ThenInResponseShouldBeAllCurrencies()
        {
            var responseRates = JsonHelper.GetResponseRates(_restResponse);

            _allSupportedCurrencies
                .All(x => responseRates.Contains(x))
                .Should()
                .BeTrue();
        }

        [Then(@"In response should be only unique selected currencies set by parameter symbols ""(.*)""")]
        public void ThenInResponseShouldBeOnlySelectedCurrenciesSetByParameterSymbols(string expectedCurrencies)
        {
            var responseRates = JsonHelper.GetResponseRates(_restResponse);
            var expectedCurrenciesAfterSplit = expectedCurrencies.GetSplittedString();

            expectedCurrenciesAfterSplit
                .Should()
                .OnlyHaveUniqueItems();

            var currenciesNotExpectedInResponseJsonBody = _allSupportedCurrencies
                .Except(expectedCurrenciesAfterSplit)
                .ToList();

            expectedCurrenciesAfterSplit
                .All(x => responseRates.Contains(x))
                .Should()
                .BeTrue();

            currenciesNotExpectedInResponseJsonBody
                .All(x => responseRates.Contains(x))
                .Should()
                .BeFalse();
        }

        [Then(@"Error should be ""(.*)""")]
        public void ThenErrorShouldBe(string expectedErrorMessage)
        {
            _jsonErrorResponse.error
                .Should()
                .Be(expectedErrorMessage);
        }

        [Then(@"Error should contain ""(.*)""")]
        public void ThenErrorShouldContain(string expectedErrorMessage)
        {
            _jsonErrorResponse.error
                .Should()
                .Contain(expectedErrorMessage);
        }

        [Then(@"In response currency value for ""(.*)"" should be (.*)")]
        public void ThenInResponseCurrencyValueForShouldBe(string currency, double expectedValue)
        {
            var value = _jsonRatesResponse.GetCurrencyValue(currency);
            Convert.ToDouble(value)
                .Should()
                .Be(expectedValue);
        }
    }
}