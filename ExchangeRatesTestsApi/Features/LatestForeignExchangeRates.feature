﻿Feature: LatestForeignExchangeRates
	Get the latest foreign exchange reference rates


Scenario: HEAD /latest endpoint is allowed
	Given I set URL to /latest
	When I send HEAD request
	Then HTTP status should be 200
	And Body should be empty


Scenario: GET /latest endpoint with the latest foreign exchange reference rates
	Given I set URL to /latest
	When I send GET request
	Then HTTP status should be 200
	And Content-Type should be "application/json"
	And Base Currency should be "EUR"
	And Date should not be empty
	#And In response should be all currencies // ignored because of bug: missing EUR-EUR https://github.com/exchangeratesapi/exchangeratesapi/issues/7


Scenario: PUT /latest endpoint is not allowed
	Given I set URL to /latest
	When I send PUT request
	Then HTTP status should be 405
	And Body should be empty


Scenario: POST /latest endpoint is not allowed
	Given I set URL to /latest
	When I send POST request
	Then HTTP status should be 405
	And Body should be "Error: Method POST not allowed for URL /latest"


Scenario: DELETE /latest endpoint is not allowed
	Given I set URL to /latest
	When I send DELETE request
	Then HTTP status should be 405
	And Body should be empty


Scenario: GET /latest with quote against a different currency by setting the base parameter in request
	Given I set URL to /latest
	When I send GET request with base parameter set to "<baseCurrency>"
	Then HTTP status should be 200
	And Content-Type should be "application/json"
	And Base Currency should be "<baseCurrency>"
	And Date should not be empty
	And In response should be all currencies

	Examples: 
	| baseCurrency |
	| USD          |
	| PLN          |
	| CZK          |
   #| EUR          | // ignored because of bug: missing EUR-EUR https://github.com/exchangeratesapi/exchangeratesapi/issues/77


Scenario: GET /latest with quote against a non-existing currency by setting the base parameter in request
	Given I set URL to /latest
	When I send GET request with non-existing base parameter set to "UUSD"
	Then HTTP status should be 400
	And Error should be "Base 'UUSD' is not supported."


Scenario: GET /latest with specific exchange rates by setting the symbols parameter in request
	Given I set URL to /latest
	When I send GET request with symbols parameter set to "<symbols>"
	Then HTTP status should be 200
	And Content-Type should be "application/json"
	And Base Currency should be "EUR"
	And Date should not be empty
	And In response should be only unique selected currencies set by parameter symbols "<symbols>"

	Examples: 
	| symbols      |
	| USD          |
	| PLN,USD      |
	| CZK,PLN,USD  |


Scenario: GET /latest with specific exchange rates by setting a non-existing symbols parameter in request
	Given I set URL to /latest
	When I send GET request with non-existing symbols parameter set to "UUSD"
	Then HTTP status should be 400
	And Error should contain "Symbols 'UUSD' are invalid"


Scenario: GET /latest with specific exchange rates by setting the base and symbols parameter in request
	Given I set URL to /latest
	When I send GET request with both symbols and base parameter set to base: "<baseCurrency>" and symbol: "<symbols>"
	Then HTTP status should be 200
	And Content-Type should be "application/json"
	And Base Currency should be "<baseCurrency>"
	And Date should not be empty
	And In response should be only unique selected currencies set by parameter symbols "<symbols>"

	Examples: 
	| baseCurrency | symbols     |
	| USD          | USD,EUR,GBP |
	| PLN          | EUR,PLN     |
	| JPY          | CZK         |


Scenario: GET /latest with specific exchange rates by setting the base and symbols empty parameter in request
	Given I set URL to /latest
	When I send GET request with both symbols and base parameter set to base: "<baseCurrency>" and symbol: ""
	Then HTTP status should be 200
	And Content-Type should be "application/json"
	And Base Currency should be "<baseCurrency>"
	And Date should not be empty
	And In response should be all currencies

	Examples: 
	| baseCurrency |
	| AUD          |
	| HUF          |


Scenario: GET /latest by setting the same base and symbols parameter in request
	Given I set URL to /latest
	When I send GET request with both symbols and base parameter set to base: "<baseCurrency>" and symbol: "<symbols>"
	Then HTTP status should be 200
	And Content-Type should be "application/json"
	And Base Currency should be "<baseCurrency>"
	And Date should not be empty
	And In response should be only unique selected currencies set by parameter symbols "<symbols>"
	And In response currency value for "<symbols>" should be 1.0

	Examples: 
	| baseCurrency | symbols |
	| USD          | USD     |
	| CZK          | CZK     |
   #| EUR          | EUR     | // ignored because of bug: missing EUR-EUR https://github.com/exchangeratesapi/exchangeratesapi/issues/77


Scenario: GET /latest returns inversely proportional currency rates value 
	Given I set URL to /latest
	When I send GET request with both symbols and base parameter set to base: "<baseCurrency>" and symbol: "<secondCurrency>"
	And I save "firstPairValue" currency rate from response for "<secondCurrency>"
	When I send GET request with both symbols and base parameter set to base: "<secondCurrency>" and symbol: "<baseCurrency>"
	And I save "secondPairValue" currency rate from response for "<baseCurrency>"
	Then HTTP status should be 200
	And Content-Type should be "application/json"
	And Date should not be empty
	And Currency rates are inversely proportional

	Examples: 
	| baseCurrency | secondCurrency |
	| USD          | EUR			|
	| PLN          | EUR			|


Scenario: GET /latest with specific exchange rates by setting the base and duplicated symbols parameter in request
	Given I set URL to /latest
	When I send GET request with both symbols and base parameter set to base: "<baseCurrency>" and symbol: "<symbols>"
	Then HTTP status should be 200
	And Content-Type should be "application/json"
	And Base Currency should be "<baseCurrency>"
	And Date should not be empty
	And In response should be only unique selected currencies set by parameter symbols "<responseSymbols>"

	Examples: 
	| baseCurrency | symbols     | responseSymbols |
	| USD          | USD,USD,EUR | USD,EUR         |
	| PLN          | CZK,PLN,CZK | CZK,PLN         |
	| JPY          | HUF,HUF     | HUF             |