﻿using RestSharp;

namespace ExchangeRatesTestsApi.Helpers
{
    public static class RestApiHelper
    {
        private const string BaseUrl = "https://api.exchangeratesapi.io";

        private const string HeaderAccept = "Header";
        private const string ApplicationJson = "application/json";

        public static RestClient SetUrl(string resourceUrl)
        {
            var url = BaseUrl + resourceUrl;
            return new RestClient(url);
        }

        public static RestRequest CreateGetRequest(string baseCurrency = null, string symbols = null)
        {
            var restRequest = new RestRequest(Method.GET);
            restRequest.AddHeader(HeaderAccept, ApplicationJson);

            if (string.IsNullOrEmpty(baseCurrency) == false)
                restRequest.AddParameter("base", baseCurrency);
            if (string.IsNullOrEmpty(symbols) == false)
                restRequest.AddParameter("symbols", symbols);

            return restRequest;
        }

        public static RestRequest CreateHeadRequest()
        {
            var restRequest = new RestRequest(Method.HEAD);
            restRequest.AddHeader(HeaderAccept, ApplicationJson);

            return restRequest;
        }

        public static RestRequest CreatePostRequest()
        {
            var restRequest = new RestRequest(Method.POST);
            restRequest.AddHeader(HeaderAccept, ApplicationJson);

            return restRequest;
        }

        public static RestRequest CreatePutRequest()
        {
            var restRequest = new RestRequest(Method.PUT);
            restRequest.AddHeader(HeaderAccept, ApplicationJson);

            return restRequest;
        }

        public static RestRequest CreateDeleteRequest()
        {
            var restRequest = new RestRequest(Method.DELETE);
            restRequest.AddHeader(HeaderAccept, ApplicationJson);

            return restRequest;
        }

        public static IRestResponse GetResponse(RestClient restClient, RestRequest restRequest)
        {
            return restClient.Execute(restRequest);
        }

        public static int GetStatusCode(this IRestResponse restResponse)
        {
            return (int) restResponse.StatusCode;
        }

        public static T GetContent<T>(this IRestResponse restResponse)
        {
            var content = restResponse.Content;

            return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(content);
        }
    }
}