﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;

namespace ExchangeRatesTestsApi.Helpers
{
    public static class JsonHelper
    {
        public static string GetResponseRates(IRestResponse restResponse)
        {
            var responseContent = restResponse.Content;
            var jObject = JObject.Parse(responseContent);
            var rates = jObject["rates"].ToString(Formatting.None);

            return rates;
        }
    }
}