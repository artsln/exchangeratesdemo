﻿namespace ExchangeRatesTestsApi.Helpers
{
    public static class StringHelper
    {
        public static string[] GetSplittedString(this string text)
        {
            return text.Trim().Split(",");
        }
    }
}