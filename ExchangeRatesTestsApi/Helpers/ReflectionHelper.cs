﻿using ExchangeRatesTestsApi.Models;

namespace ExchangeRatesTestsApi.Helpers
{
    public static class ReflectionHelper
    {
        public static string GetCurrencyValue(this RatesResponse jsonRatesResponse,  string currency)
        {
            return jsonRatesResponse.rates
                .GetType()
                .GetProperty(currency)
                ?.GetValue(jsonRatesResponse.rates)
                ?.ToString();
        }
    }
}